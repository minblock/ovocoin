Building Ovo
=============

See doc/build-*.md for instructions on building the various
elements of the Ovo Core reference implementation of Ovo.
