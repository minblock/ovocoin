Sample configuration files for:

SystemD: ovod.service
Upstart: ovod.conf
OpenRC:  ovod.openrc
         ovod.openrcconf
CentOS:  ovod.init
OS X:    org.ovo.ovod.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
